package edu.javasoft.cinema.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import edu.javasoft.cinema.model.Movie;
import edu.javasoft.cinema.model.Ticket;
import edu.javasoft.cinema.model.User;
import edu.javasoft.cinema.repositiry.TicketRepository;

public class CinemaService {

	private List<Ticket> ticketList;

	public CinemaService() {
		this.ticketList = new TicketRepository().getTicketList();
	}
	
	public void setTicketList(List<Ticket> ticketList) {
		this.ticketList = ticketList;
	}

	public List<Ticket> getTicketList() {
		return ticketList;
	}

	public List<Movie> getTodayMovies() {
		return ticketList.stream()
				.filter(a -> a.getDate().toLocalDate().equals(LocalDate.now()))
				.map(Ticket::getMovie)
				.distinct()
				.sorted((a, b) -> a.getTitle().compareTo(b.getTitle()))
				.collect(Collectors.toList());
	}

	public List<User> getUsersByDateAndMovie(Movie movie, LocalDate date) {
		return ticketList.stream()
				.filter(a -> a.getDate().toLocalDate().equals(date) && a.getMovie().equals(movie))
				.map(Ticket::getUser)
				.sorted((a, b) -> a.getName().compareTo(b.getName()))
				.collect(Collectors.toList());
	}

	public Double getMonthIncome(int year, int month) {
		return ticketList.stream()
				.filter(a -> a.getDate().getYear() == year && a.getDate().getMonthValue() == month)
				.map(Ticket::getPrice)
				.reduce(0.0, Double::sum);
	}

	public List<Movie> getMoviesSortedByVisitingCount() {
		
		return ticketList.stream()
				.collect(Collectors.groupingBy(Ticket::getMovie, Collectors.counting()) )
				.entrySet().stream()
				.sorted(Map.Entry.<Movie, Long>comparingByValue().thenComparing(a -> a.getKey().getTitle()).reversed())
				.map(Map.Entry::getKey)
				.collect(Collectors.toList());
				
	}

	public List<Movie> getMoviesThatLessCertainTicketAmount(int ticketAmount) {
		return ticketList.stream()
				.collect(Collectors.groupingBy(Ticket::getMovie, Collectors.counting()))
				.entrySet().stream()
				.sorted(Map.Entry.<Movie, Long>comparingByValue().thenComparing(a -> a.getKey().getTitle()).reversed())
				.filter(a -> a.getValue() < ticketAmount)
				.map(Map.Entry::getKey)
				.collect(Collectors.toList());
	}
}
