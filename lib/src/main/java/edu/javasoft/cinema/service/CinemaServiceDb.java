package edu.javasoft.cinema.service;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.javasoft.cinema.model.Genre;
import edu.javasoft.cinema.model.Movie;
import edu.javasoft.cinema.model.User;
import edu.javasoft.cinema.utility.DbConnectionUtility;

/**
 * The CinemaServiceDb class provides tools and services for data processing from cinema database
 * @author Serhii Kondriuchok
 *
 */
public class CinemaServiceDb {
	
	private static final Logger LOGGER = LogManager.getLogger(CinemaServiceDb.class);

	/**
	 * Initializes a newly created CinemaServiceDb object
	 */
	public CinemaServiceDb() {
	}

	/**
	 * Returns a list of movies which are shown today
	 * @return list of movies
	 */
	public List<Movie> getTodayMovies() {
		
		LOGGER.info("invoked method getTodayMovies()");
		
		final String SQL_GET_TODAY_MOVIES = "SELECT DISTINCT title, duration, genres, rating "
				+ "FROM tickets "
				+ "INNER JOIN movies ON title = movie_title "
				+ "WHERE date(show_date) = CURRENT_DATE "
				+ "ORDER BY title";
		
		List<Movie> movies = new ArrayList<>();
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_GET_TODAY_MOVIES);
				ResultSet rs = pstmt.executeQuery()) {
			
			Movie movie = null;
			
			while (rs.next()) {
				
				String title = rs.getString("title");
				Duration duration = Duration.ofMinutes(rs.getLong("duration"));
				List<Genre> genres = Arrays.stream((String[]) rs.getArray("genres").getArray())
						.map(Genre::valueOf)
						.collect(Collectors.toList());
				Double rating = rs.getDouble("rating");
				
				movie = new Movie(title, duration, genres, rating);
				
				movies.add(movie);
			}
			
			if (movies.isEmpty()) {
				LOGGER.warn("list with today movies is empty");
			}
			
			return movies;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while getting today movies from database", e);
			return movies;
		}
	}
	
	/**
	 * Returns a list of users who have bought tickets on a certain movie and date
	 * @param movie a movie
	 * @param date a show date
	 * @return list of users
	 */
	public List<User> getUsersByDateAndMovie(Movie movie, LocalDate date) {
		
		LOGGER.info("invoked method getUsersByDateAndMovie() with parameters 'movie' = {} and 'date' = {}", movie, date);
		
		final String SQL_GET_USERS_BY_DATE_AND_MOVIE = "SELECT name, phone "
				+ "FROM tickets "
				+ "INNER JOIN users ON phone = user_phone "
				+ "WHERE date(show_date) = ? AND movie_title = ?";
		
		ResultSet rs = null;
		
		List<User> users = new ArrayList<>();
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_GET_USERS_BY_DATE_AND_MOVIE)) {
			
			pstmt.setDate(1, Date.valueOf(date));
			pstmt.setString(2, movie.getTitle());
			
			rs = pstmt.executeQuery();
			
			User user = null;
			
			while (rs.next()) {
				
				String name = rs.getString("name");
				String phone = rs.getString("phone");
				
				user = new User(name, phone);
				
				users.add(user);
			}
			
			if (users.isEmpty()) {
				LOGGER.warn("list with users is empty");
			}
			
			return users;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while getting user from database", e);
			return users;
		} finally {
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				LOGGER.error("SQLException occurred while closing ResultSet", e);
			}
		}
	}
	
	/**
	 * Returns an income value of a certain month in a year
	 * @param year an integer, value of a year
	 * @param month an integer, value of a month (from 1 to 12)
	 * @return returns an income value
	 */
	public Double getMonthIncome(int year, int month) {
		
		LOGGER.info("invoked method getMonthIncome() with parameters 'year' = {} and 'month' = {}", year, month);
		
		final String SQL_GET_MONTH_INCOME = "SELECT sum(price) AS month_income "
				+ "FROM tickets "
				+ "WHERE EXTRACT(YEAR FROM show_date) = ? AND EXTRACT(MONTH FROM show_date) = ?";
		
		ResultSet rs = null;
		
		Double income = 0.0;
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_GET_MONTH_INCOME)) {
			
			pstmt.setInt(1, year);
			pstmt.setInt(2, month);
			
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				income = rs.getDouble("month_income");
			}
			
			if (income == 0.0) {
				LOGGER.warn("month income is zero");
			}
			
			return income;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while getting month income", e);
			return income;
		} finally {
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				LOGGER.error("SQLException occurred while closing ResultSet", e);
			}
		}
	}
	
	/**
	 * Returns a list of movies sorted by sold tickets
	 * @return list of movies
	 */
	public List<Movie> getMoviesSortedByVisitingCount() {
		
		LOGGER.info("invoked method getMoviesSortedByVisitingCount()");
		
		final String SQL_GET_MOVIES_SORTED_BY_VISITING_COUNT = "SELECT title, duration, genres, rating "
				+ "FROM tickets "
				+ "INNER JOIN movies ON movie_title = title "
				+ "GROUP BY title "
				+ "ORDER BY count(*) DESC";
		
		List<Movie> movies = new ArrayList<>();
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_GET_MOVIES_SORTED_BY_VISITING_COUNT);
				ResultSet rs = pstmt.executeQuery()) {
			
			Movie movie = null;
			
			while (rs.next()) {
				
				String title = rs.getString("title");
				Duration duration = Duration.ofMinutes(rs.getLong("duration"));
				List<Genre> genres = Arrays.stream((String[]) rs.getArray("genres").getArray())
						.map(Genre::valueOf)
						.collect(Collectors.toList());
				Double rating = rs.getDouble("rating");
				
				movie = new Movie(title, duration, genres, rating);
				
				movies.add(movie);
			}
			
			if (movies.isEmpty()) {
				LOGGER.warn("list with sorted movies is empty");
			}
			
			return movies;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while getting movies from database", e);
			return movies;
		}
				
	}
	
	/**
	 * Returns a list of movies which have number of sold tickets less than certain amount 
	 * @param ticketAmount total number of sold tickets
	 * @return list of movies
	 */
	public List<Movie> getMoviesThatLessCertainTicketAmount(int ticketAmount) {
		
		LOGGER.info("invoked method getMoviesThatLessCertainTicketAmount() with parameter 'ticketAmount' = {}", ticketAmount);
		
		final String SQL_GET_MOVIES_THAT_LESS_CERTAIN_TICKET_AMOUNT = "SELECT title, duration, genres, rating "
				+ "FROM tickets "
				+ "INNER JOIN movies ON movie_title = title "
				+ "GROUP BY title "
				+ "HAVING count(*) < ? "
				+ "ORDER BY count(*) DESC";
		
		ResultSet rs = null;
		
		List<Movie> movies = new ArrayList<>();
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_GET_MOVIES_THAT_LESS_CERTAIN_TICKET_AMOUNT)) {
			
			pstmt.setInt(1, ticketAmount);
			
			rs = pstmt.executeQuery();
			
			Movie movie = null;
			
			while (rs.next()) {
				
				String title = rs.getString("title");
				Duration duration = Duration.ofMinutes(rs.getLong("duration"));
				List<Genre> genres = Arrays.stream((String[]) rs.getArray("genres").getArray())
						.map(Genre::valueOf)
						.collect(Collectors.toList());
				Double rating = rs.getDouble("rating");
				
				movie = new Movie(title, duration, genres, rating);
				
				movies.add(movie);
			}
			
			if (movies.isEmpty()) {
				LOGGER.warn("list with movies is empty");
			}
			
			return movies;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while getting movies from database", e);
			return movies;
		} finally {
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				LOGGER.error("SQLException occurred while closing ResultSet", e);
			}
		}
	}
	

}