package edu.javasoft.cinema.repositiry;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.javasoft.cinema.model.User;
import edu.javasoft.cinema.utility.RepositoryUtility;

public class UserRepository {
	
	private static final Logger LOGGER = LogManager.getLogger(UserRepository.class);

	private static final String JSON_USERS_FILE = "src/main/resources/users.json";
	
	private List<User> userList;

	public UserRepository() {
		LOGGER.info("reading users from file '{}'", JSON_USERS_FILE);
		try {
			this.userList = RepositoryUtility.getListFromFile(JSON_USERS_FILE, User.class);
		} catch(IOException e) {
			LOGGER.error("IOException occurred while reading data from file {}", e.getMessage());
		}
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
	
	public boolean addUser(User user) {
		
		for (User u : userList) {
			if (u.equals(user) ) {
				return false;
			}
		}
		
		return userList.add(user);
	}
	
	public User getUser(String userPhone) {
		
		for (User u : userList) {
			if (u.getPhone().equals(userPhone)) {
				return u;
			}
		}
		
		return null;
	}
	
	public boolean updateUser(User user) {
		for (User u : userList) {
			if (u.equals(user)) {
				u.setName(user.getName());
				return true;
			}
		}
		return false;
	}
	
	public boolean removeUser(String userPhone) {
		for (User u : userList) {
			if (u.getPhone().equals(userPhone)) {
				return userList.remove(u);
			}
		}
		return false;
	}
	
	public void save() {
		LOGGER.info("saving users to file '{}'", JSON_USERS_FILE);
		try {
			RepositoryUtility.saveList(userList, JSON_USERS_FILE);
		} catch(IOException e) {
			LOGGER.error("IOException occurred while saving data to file {}", e.getMessage());
		}
	}
}
