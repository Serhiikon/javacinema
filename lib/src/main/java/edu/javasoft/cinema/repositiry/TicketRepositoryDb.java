package edu.javasoft.cinema.repositiry;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.javasoft.cinema.model.Genre;
import edu.javasoft.cinema.model.Movie;
import edu.javasoft.cinema.model.Ticket;
import edu.javasoft.cinema.model.User;
import edu.javasoft.cinema.utility.DbConnectionUtility;

public class TicketRepositoryDb {
	
	private static final Logger LOGGER = LogManager.getLogger(TicketRepositoryDb.class);

	public boolean addTicket(Ticket ticket) {
		
		LOGGER.info("invoked method addTicket() with parameter 'ticket' = {}", ticket);
		
		final String SQL_INSERT = "INSERT INTO tickets (id, movie_title, user_phone, show_date, price) "
				+ "VALUES (?, ?, ?, ?, ?)";
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_INSERT)) {
			
			pstmt.setObject(1, ticket.getId());
			pstmt.setString(2, ticket.getMovie().getTitle());
			pstmt.setString(3, ticket.getUser().getPhone());
			pstmt.setTimestamp(4, Timestamp.valueOf(ticket.getDate()));
			pstmt.setDouble(5, ticket.getPrice());
			
			int rowCount = pstmt.executeUpdate();
			
			if (rowCount == 0) {
				LOGGER.warn("ticket {} has not been added to database", ticket.getId());
				return false;
			}
			return true;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while adding ticket to database", e);
			return false;
		}
	}
	
	public Ticket getTicket(UUID ticketId) {
		
		LOGGER.info("invoked method getTicket() with parameter 'ticketId' = {}", ticketId);
		
		final String SQL_GET = "SELECT * "
				+ "FROM tickets "
				+ "INNER JOIN movies ON movie_title = title "
				+ "INNER JOIN users ON user_phone = phone "
				+ "WHERE id = ?";
		
		ResultSet rs = null;
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_GET)) {
			
			pstmt.setObject(1, ticketId);
			
			rs = pstmt.executeQuery();
			
			Ticket ticket = null;
			
			if (rs.next()) {
				
				UUID id = (UUID) rs.getObject("id");
				
				String title = rs.getString("title");
				Duration duration = Duration.ofMinutes(rs.getLong("duration"));
				List<Genre> genres = Arrays.stream((String[]) rs.getArray("genres").getArray())
						.map(Genre::valueOf)
						.collect(Collectors.toList());
				Double rating = rs.getDouble("rating");
				Movie movie = new Movie(title, duration, genres, rating);
				
				String name = rs.getString("name");
				String phone = rs.getString("phone");
				User user = new User(name, phone);
				
				LocalDateTime date = rs.getTimestamp("show_date").toLocalDateTime();
				
				Double price = rs.getDouble("price");
				
				ticket = new Ticket(id, movie, user, date, price);
			}
			
			if (ticket == null) {
				LOGGER.warn("ticket with id {} is not found", ticketId);
			}
			
			return ticket;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while getting ticket from database", e);
			return null;
		} finally {
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				LOGGER.error("SQLException occurred while closing ResultSet", e);
			}
		}
	}
	
	public boolean updateTicket(Ticket ticket) {
		
		LOGGER.info("invoked method updateTicket() with parameter 'ticket' = {}", ticket);
		
		final String SQL_UPDATE = "UPDATE tickets SET movie_title = ?, user_phone = ?, show_date = ?, price = ? WHERE id = ?";
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_UPDATE)) {
			
			pstmt.setString(1, ticket.getMovie().getTitle());
			pstmt.setString(2, ticket.getUser().getPhone());
			pstmt.setTimestamp(3, Timestamp.valueOf(ticket.getDate()));
			pstmt.setDouble(4, ticket.getPrice());
			pstmt.setObject(5, ticket.getId());
			
			int rowCount = pstmt.executeUpdate();
			
			if (rowCount == 0) {
				LOGGER.warn("ticket with id {} has not been updated", ticket.getId());
				return false;
			}
			return true;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while updating ticket", e);
			return false;
		}
	}
	
	public boolean removeTicket(UUID ticketId) {
		
		LOGGER.info("invoked method removeTicket() with parameter 'ticketId' = {}", ticketId);
		
		final String SQL_REMOVE = "DELETE FROM tickets WHERE id = ?";
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_REMOVE)) {
			
			pstmt.setObject(1, ticketId);
			
			int rowCount = pstmt.executeUpdate();
			
			if (rowCount == 0) {
				LOGGER.warn("ticket with id {} has not been removed", ticketId);
				return false;
			}
			return true;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while removing ticket from database", e);
			return false;
		}
	}
}
