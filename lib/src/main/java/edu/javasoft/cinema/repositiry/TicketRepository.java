package edu.javasoft.cinema.repositiry;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.javasoft.cinema.model.Ticket;
import edu.javasoft.cinema.utility.RepositoryUtility;

public class TicketRepository {
	
	private static final Logger LOGGER = LogManager.getLogger(TicketRepository.class);
	
	private static final String JSON_TICKETS_FILE = "src/main/resources/tickets.json";

	private List<Ticket> ticketList;

	public TicketRepository() {
		LOGGER.info("reading tickets from file '{}'", JSON_TICKETS_FILE);
		try {
			this.ticketList = RepositoryUtility.getListFromFile(JSON_TICKETS_FILE, Ticket.class);
		} catch(IOException e) {
			LOGGER.error("IOException occurred while reading data from file {}", e.getMessage());
		}
	}

	public List<Ticket> getTicketList() {
		return ticketList;
	}

	public void setTicketList(List<Ticket> ticketList) {
		this.ticketList = ticketList;
	}
	
	public boolean addTicket(Ticket ticket) {
		
		for (Ticket t : ticketList) {
			if (t.equals(ticket) ) {
				return false;
			}
		}
		
		return ticketList.add(ticket);
	}
	
	public Ticket getTicket(UUID id) {
		
		for (Ticket t : ticketList) {
			if (t.getId().equals(id)) {
				return t;
			}
		}
		
		return null;
	}
	
	public boolean updateTicket(Ticket ticket) {
		for (Ticket t : ticketList) {
			if (t.equals(ticket)) {
				t.setMovie(ticket.getMovie());
				t.setPrice(ticket.getPrice());
				t.setDate(ticket.getDate());
				t.setUser(ticket.getUser());
				return true;
			}
		}
		return false;
	}
	
	public boolean removeTicket(UUID id) {
		for (Ticket t : ticketList) {
			if (t.getId().equals(id)) {
				return ticketList.remove(t);
			}
		}
		return false;
	}
	
	public void save() {
		LOGGER.info("saving tickets to file '{}'", JSON_TICKETS_FILE);
		try {
			RepositoryUtility.saveList(ticketList, JSON_TICKETS_FILE);
		} catch(IOException e) {
			LOGGER.error("IOException occurred while saving data to file {}", e.getMessage());
		}
	}
}
