package edu.javasoft.cinema.repositiry;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.javasoft.cinema.model.User;
import edu.javasoft.cinema.utility.DbConnectionUtility;

public class UserRepositoryDb {
	
	private static final Logger LOGGER = LogManager.getLogger(UserRepositoryDb.class);

	public boolean addUser(User user) {
		
		LOGGER.info("invoked method addUser() with parameter 'user' = {}", user);
		
		final String SQL_INSERT = "INSERT INTO users (name, phone) VALUES (?, ?)";
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_INSERT)) {
			
			pstmt.setString(1, user.getName());
			pstmt.setString(2, user.getPhone());
			
			int rowCount = pstmt.executeUpdate();
			
			if (rowCount == 0) {
				LOGGER.warn("user {} has not been added to database", user.getName());
				return false;
			}
			return true;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while adding user to database", e);
			return false;
		}
		
	}
	
	public User getUser(String userPhone) {
		
		LOGGER.info("invoked method getUser() with parameter 'phone' = {}", userPhone);
		
		final String SQL_GET = "SELECT * FROM users WHERE phone = ?";
		
		ResultSet rs = null;
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_GET)) {
			
			pstmt.setString(1, userPhone);
			
			rs = pstmt.executeQuery();
			
			User user = null;
			
			if (rs.next()) {
				
				String name = rs.getString("name");
				String phone = rs.getString("phone");
				
				user = new User(name, phone);
			}
			
			if (user == null) {
				LOGGER.warn("user with phone number {} is not found", userPhone);
			}
			
			return user;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while getting user from database", e);
			return null;
		} finally {
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				LOGGER.error("SQLException occurred while closing ResultSet", e);
			}
		}
	}
	
	public boolean updateUser(User user) {
		
		LOGGER.info("invoked method updateUser() with parameter 'user' = {}", user);
		
		final String SQL_UPDATE = "UPDATE users SET name = ? WHERE phone = ?";
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_UPDATE)) {
			
			pstmt.setString(1, user.getName());
			pstmt.setString(2, user.getPhone());
			
			int rowCount = pstmt.executeUpdate();
			
			if (rowCount == 0) {
				LOGGER.warn("user {} has not been updated", user.getName());
				return false;
			}
			return true;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while updating user", e);
			return false;
		}
	}
	
	public boolean removeUser(String userPhone) {
		
		LOGGER.info("invoked method removeUser() with parameter 'phone' = {}", userPhone);
		
		final String SQL_REMOVE = "DELETE FROM users WHERE phone = ?";
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_REMOVE)) {
			
			pstmt.setString(1, userPhone);
			
			int rowCount = pstmt.executeUpdate();
			
			if (rowCount == 0) {
				LOGGER.warn("user with phone {} has not been removed", userPhone);
				return false;
			}
			return true;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while removing user from database", e);
			return false;
		}
	}
}
