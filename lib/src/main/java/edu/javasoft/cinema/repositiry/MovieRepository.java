package edu.javasoft.cinema.repositiry;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.javasoft.cinema.model.Movie;
import edu.javasoft.cinema.utility.RepositoryUtility;

public class MovieRepository {
	
	private static final Logger LOGGER = LogManager.getLogger(MovieRepository.class);
	
	private static final String JSON_MOVIES_FILE = "src/main/resources/movies.json";
	
	private List<Movie> movieList;

	public MovieRepository () {
		LOGGER.info("reading movies from file '{}'", JSON_MOVIES_FILE);
		try {
			this.movieList = RepositoryUtility.getListFromFile(JSON_MOVIES_FILE, Movie.class);
		} catch(IOException e) {
			LOGGER.error("IOException occurred while reading data from file {}", e.getMessage());
		}
	}

	public List<Movie> getMovieList() {
		return movieList;
	}

	public void setMovieList(List<Movie> movieList) {
		this.movieList = movieList;
	}

	public boolean addMovie(Movie movie) {
		
		for (Movie m : movieList) {
			if (m.equals(movie) ) {
				return false;
			}
		}
		
		return movieList.add(movie);
	}
	
	public Movie getMovie(String title) {
		
		for (Movie m : movieList) {
			if (m.getTitle().equals(title) ) {
				return m;
			}
		}
		
		return null;
	}
	
	public boolean updateMovie(Movie movie) {
		for (Movie m : movieList) {
			if (m.equals(movie)) {
				m.setDuration(movie.getDuration());
				m.setGenres(movie.getGenres());
				m.setRating(movie.getRating());
				return true;
			}
		}
		return false;
	}
	
	public boolean removeMovie(String title) {
		for (Movie m : movieList) {
			if (m.getTitle().equals(title) ) {
				return movieList.remove(m);
			}
		}
		return false;
	}
	
	public void save() {
		LOGGER.info("saving movies to file '{}'", JSON_MOVIES_FILE);
		try {
			RepositoryUtility.saveList(movieList, JSON_MOVIES_FILE);
		} catch(IOException e) {
			LOGGER.error("IOException occurred while saving data to file {}", e.getMessage());
		}
	}
}
