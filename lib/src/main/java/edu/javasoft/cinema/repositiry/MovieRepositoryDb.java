package edu.javasoft.cinema.repositiry;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.javasoft.cinema.model.Genre;
import edu.javasoft.cinema.model.Movie;
import edu.javasoft.cinema.utility.DbConnectionUtility;

public class MovieRepositoryDb {
	
	private static final Logger LOGGER = LogManager.getLogger(MovieRepositoryDb.class);

	public boolean addMovie(Movie movie) {
		
		LOGGER.info("invoked method addMovie() with parameter 'movie' = {}", movie);
		
		final String SQL_INSERT = "INSERT INTO movies (title, duration, genres, rating) VALUES (?, ?, ?, ?)";
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_INSERT)) {
			
			pstmt.setString(1, movie.getTitle());
			pstmt.setLong(2, movie.getDuration().toMinutes());
			pstmt.setArray(3, conn.createArrayOf("varchar", movie.getArrayOfGenres()));
			pstmt.setDouble(4, movie.getRating());
			
			int rowCount = pstmt.executeUpdate();
			
			if (rowCount == 0) {
				LOGGER.warn("movie {} has not been added to database", movie.getTitle());
				return false;
			}
			return true;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while adding movie to database", e);
			return false;
		}
		
	}
	
	public Movie getMovie(String movieTitle) {
		
		LOGGER.info("invoked method getMovie() with parameter 'title' = {}", movieTitle);
		
		final String SQL_GET = "SELECT * FROM movies WHERE title = ?";
		
		ResultSet rs = null;
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_GET)) {
			
			pstmt.setString(1, movieTitle);
			
			rs = pstmt.executeQuery();
			
			Movie movie = null;
			
			if (rs.next()) {
				
				String title = rs.getString("title");
				Duration duration = Duration.ofMinutes(rs.getLong("duration"));
				List<Genre> genres = Arrays.stream((String[]) rs.getArray("genres").getArray())
						.map(Genre::valueOf)
						.collect(Collectors.toList());
				Double rating = rs.getDouble("rating");
				
				movie = new Movie(title, duration, genres, rating);
			}
			
			if (movie == null) {
				LOGGER.warn("movie with title {} is not found", movieTitle);
			}
			
			return movie;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while getting movie from database", e);
			return null;
		} finally {
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				LOGGER.error("SQLException occurred while closing ResultSet", e);
			}
		}
	}
	
	public boolean updateMovie(Movie movie) {
		
		LOGGER.info("invoked method updateMovie() with parameter 'movie' = {}", movie);
		
		final String SQL_UPDATE = "UPDATE movies SET duration = ?, genres = ?, rating = ? WHERE title = ?";
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_UPDATE)) {
			
			pstmt.setLong(1, movie.getDuration().toMinutes());
			pstmt.setArray(2, conn.createArrayOf("varchar", movie.getArrayOfGenres()));
			pstmt.setDouble(3, movie.getRating());
			pstmt.setString(4, movie.getTitle());
			
			int rowCount = pstmt.executeUpdate();
			
			if (rowCount == 0) {
				LOGGER.warn("movie {} has not been updated", movie.getTitle());
				return false;
			}
			return true;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while updating movie", e);
			return false;
		}
	}
	
	public boolean removeMovie(String title) {
		
		LOGGER.info("invoked method removeMovie() with parameter 'title' = {}", title);
		
		final String SQL_REMOVE = "DELETE FROM movies WHERE title = ?";
		
		try (Connection conn = DbConnectionUtility.getDbConnection();
				PreparedStatement pstmt = conn.prepareStatement(SQL_REMOVE)) {
			
			pstmt.setString(1, title);
			
			int rowCount = pstmt.executeUpdate();
			
			if (rowCount == 0) {
				LOGGER.warn("movie with title {} has not been removed", title);
				return false;
			}
			return true;
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occurred while removing movie from database", e);
			return false;
		}
	}
}
