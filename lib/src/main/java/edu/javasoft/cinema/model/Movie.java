package edu.javasoft.cinema.model;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import edu.javasoft.cinema.utility.ObjectValidator;

public class Movie {
	
	@NotEmpty(message = "movie title and duration is required fields")
	private String title;
	
	@NotNull(message = "movie title and duration is required fields")
	private Duration duration;
	
	private List<Genre> genres;
	
	@Min(value = 0, message = "movie rating must be within range 0.0 .. 10.0")
    @Max(value = 10, message = "movie rating must be within range 0.0 .. 10.0")
	private Double rating;
	
	public Movie() {
	}

	public Movie(String title, Duration duration, List<Genre> genres, Double rating) {
		this.title = title;
		this.duration = duration;
		this.genres = genres;
		this.rating = rating;
	}

	public String[] getArrayOfGenres() {
		return Arrays.stream(genres.toArray())
				.map(Object::toString)
				.toArray(String[]::new);
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
		new ObjectValidator<Movie>(this).runValidation();
	}

	@JsonSetter("title")
	private void setJsonTitle(String title) {
		this.title = title;
	}
	
	public Duration getDuration() {
		return duration;
	}
	
	@JsonGetter("duration")
	private long getJsonDuration() {
		return duration.toMinutes();
	}

	public void setDuration(Duration duration) {
		this.duration = duration;
		new ObjectValidator<Movie>(this).runValidation();
	}
	
	@JsonSetter("duration")
	private void setJsonDuration(long duration) {
		this.duration = Duration.ofMinutes(duration);
	}

	public List<Genre> getGenres() {
		return genres;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
		new ObjectValidator<Movie>(this).runValidation();
	}
	
	@JsonSetter("rating")
	private void setJsonRating(Double rating) {
		this.rating = rating;
	}
	
	public void addGenre(Genre genre) {
		this.genres.add(genre);
	}
	
	public static class Builder {
		
		private Movie newMovie;
		
		public Builder() {
			newMovie = new Movie();
		}
		
		public Builder withTitle(String title) {
			newMovie.title = title;
			return this;
		}
		
		public Builder withDuration(Duration duration) {
			newMovie.duration = duration;
			return this;
		}
		
		public Builder withGenres(List<Genre> genres) {
			newMovie.genres = genres;
			return this;
		}
		
		public Builder withRating(Double rating) {
			newMovie.rating = rating;
			return this;
		}
		
		public Movie build() {
			new ObjectValidator<Movie>(newMovie).runValidation();
			return newMovie;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(title);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movie other = (Movie) obj;
		return Objects.equals(title, other.title);
	}

	@Override
	public String toString() {
		return "\nMovie [title=" + title + ", duration=" + duration.toMinutes() + ", genres=" + genres + ", rating=" + rating + "]";
	}
	
	
}
