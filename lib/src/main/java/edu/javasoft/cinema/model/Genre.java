package edu.javasoft.cinema.model;

public enum Genre {

	ACTION, COMEDY, DRAMA,
	FANTASY, HORROR, MYSTERY,
	ROMANCE, THRILLER, WESTERN,
	ADVENTURE, SCI_FI,  CRIME,
	HISTORY;
	
	private String description;

	public String getDescription() {
		return description;
	}

	void setDescription(String description) {
		this.description = description;
	}
	
}
