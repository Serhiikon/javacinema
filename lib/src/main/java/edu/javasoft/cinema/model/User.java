package edu.javasoft.cinema.model;

import java.util.Objects;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonSetter;

import edu.javasoft.cinema.utility.ObjectValidator;

public class User {
	
	@NotEmpty(message = "user name is required field")
	private String name;
	
	@Pattern(regexp = "^\\(\\d{3}\\)\\s\\d{3}-\\d{2}-\\d{2}$", 
			message = "user phone must be written by this pattern: (xxx) xxx-xx-xx")
	private String phone;

	public User() {
	}
	
	public User(String name, String phone) {
		this.name = name;
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		new ObjectValidator<User>(this).runValidation();
	}
	
	@JsonSetter("name")
	private void setJsonName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
		new ObjectValidator<User>(this).runValidation();
	}
	
	@JsonSetter("phone")
	private void setJsonPhone(String phone) {
		this.phone = phone;
	}
	
	public static class Builder {
		
		private User newUser;
		
		public Builder() {
			newUser = new User();
		}
		
		public Builder withName (String name) {
			newUser.name = name;
			return this;
		}
		
		public Builder withPhone (String phone) {
			newUser.phone = phone;
			return this;
		}
		
		public User build() {
			new ObjectValidator<User>(newUser).runValidation();
			return newUser;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(phone);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return Objects.equals(phone, other.phone);
	}

	@Override
	public String toString() {
		return "\nUser [name=" + name + ", phone=" + phone + "]";
	}
	
	
}
