package edu.javasoft.cinema.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import edu.javasoft.cinema.exception.ViolatingObjectException;
import edu.javasoft.cinema.utility.ObjectValidator;

public class Ticket {
	
	private static final String DATE_PATTERN = "dd/MM/yyyy, HH:mm:ss";

	@NotNull(message = "ID is required fields")
	private UUID id;
	
	@NotNull(message = "movie and price is required fields")
	private Movie movie;
	
	private User user;
	
	private LocalDateTime date;
	
	@NotNull(message = "movie and price is required fields")
	private Double price;

	public Ticket() {
		this.id = UUID.randomUUID();
		this.date = LocalDateTime.now();
	}
	
	public Ticket(UUID id, Movie movie, User user, LocalDateTime date, Double price) {
		this.id = id;
		this.movie = movie;
		this.user = user;
		this.date = date;
		this.price = price;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
		new ObjectValidator<Ticket>(this).runValidation();
	}
	
	@JsonSetter("id")
	private void setJsonId(String id) {
		this.id = UUID.fromString(id);
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
		new ObjectValidator<Ticket>(this).runValidation();
	}
	
	@JsonSetter("movie")
	private void setJsonMovie(Movie movie) {
		this.movie = movie;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public LocalDateTime getDate() {
		return date;
	}
	
	@JsonGetter("date")
	private String getJsonDate() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
		return date.format(formatter);
	}

	public void setDate(LocalDateTime date) {
		if (date.isBefore(this.date)) {
			throw new ViolatingObjectException("ticket date must be in the present or in the future");
		}
		this.date = date;
	}
	
	@JsonSetter("date")
	private void setJsonDate(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
		this.date = LocalDateTime.parse(date, formatter);
	}
	
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
		new ObjectValidator<Ticket>(this).runValidation();
	}
	
	@JsonSetter("price")
	private void setJsonPrice(Double price) {
		this.price = price;
	}

	public static class Builder {
		
		private Ticket newTicket;
		
		public Builder() {
			newTicket = new Ticket();
		}
		
		public Builder withId(UUID id) {
			newTicket.id = id;
			return this;
		}
		
		public Builder withMovie(Movie movie) {
			newTicket.movie = movie;
			return this;
		}
		
		public Builder withUser(User user) {
			newTicket.user = user;
			return this;
		}
		
		public Builder withDate(LocalDateTime date) {
			if (date.isBefore(newTicket.date)) {
				throw new ViolatingObjectException("ticket date must be in the present or in the future");
			}
			newTicket.date = date;
			return this;
		}
		
		public Builder withPrice(Double price) {
			newTicket.price = price;
			return this;
		}
		
		public Ticket build() {
			new ObjectValidator<Ticket>(newTicket).runValidation();
			return newTicket;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ticket other = (Ticket) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
		String strDate = date.format(formatter);
		return "\nTicket [id=" + id + ", movie=" + movie + ", user=" + user + ", date=" + strDate + ", price=" + price + "]";
	}
	
	
}
