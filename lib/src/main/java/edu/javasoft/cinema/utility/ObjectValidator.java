package edu.javasoft.cinema.utility;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import edu.javasoft.cinema.exception.ViolatingObjectException;

public class ObjectValidator<T> {

	private T obj;

	public ObjectValidator(T obj) {
		this.obj = obj;
	}
	
	public void runValidation() {
		
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		
		Set<ConstraintViolation<T>> violations = validator.validate(obj);
		
		StringBuilder sb = new StringBuilder();
		
		for (ConstraintViolation<T> violation : violations) {
			sb.append("\n" + violation.getMessage() + ";");
		}
		
		if (sb.length() != 0) throw new ViolatingObjectException(sb.toString());
		
		
		
	}
}
