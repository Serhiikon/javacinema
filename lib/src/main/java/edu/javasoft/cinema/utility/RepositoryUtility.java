package edu.javasoft.cinema.utility;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

public class RepositoryUtility {
	
	private RepositoryUtility() {
	}
	
	public static <T> List<T> getListFromFile(String fileName, Class<T> type) throws IOException {
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		File file = new File(fileName);
		
		List<T> objectList = new ArrayList<>();
		
		if (file.length() != 0) { 
		
			objectList = objectMapper.readValue(file, TypeFactory.defaultInstance().constructCollectionType(ArrayList.class, type));
		
		}
		
		return objectList;
	}

	public static <T> void saveList(T objectList, String fileName) throws IOException {
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		objectMapper.writeValue(new File(fileName), objectList);
	}
	
}
