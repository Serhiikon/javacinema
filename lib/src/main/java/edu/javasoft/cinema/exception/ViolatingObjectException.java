package edu.javasoft.cinema.exception;

public class ViolatingObjectException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ViolatingObjectException(String message) {
		super(message);
	}

	
}
