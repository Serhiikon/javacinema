package edu.javasoft.cinema.repositiry;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import edu.javasoft.cinema.model.Genre;
import edu.javasoft.cinema.model.Movie;

public class MovieRepositoryTest {
	
	private MovieRepository movieRepository;
	
	@BeforeMethod
	public void beforeMethod() {

		Movie movie = new Movie("The Shawshank Redemption", Duration.ofMinutes(142), Arrays.asList(Genre.DRAMA), 9.3);

		List<Movie> movieList = new ArrayList<>();

		movieList.add(movie);

		this.movieRepository = new MovieRepository();
		this.movieRepository.setMovieList(movieList);
	}

	@Test
	public void addMovieTest() {
		
		List<Movie> expectedMovies = new ArrayList<>();
		
		Movie movie1 = new Movie("The Shawshank Redemption", Duration.ofMinutes(142), Arrays.asList(Genre.DRAMA), 9.3);
		
		expectedMovies.add(movie1);
		
		Movie movie2 = new Movie("The Godfather", Duration.ofMinutes(175), Arrays.asList(Genre.DRAMA), 9.2);
		
		expectedMovies.add(movie2);
		
		movieRepository.addMovie(movie2);
		
		List<Movie> actualMovies = movieRepository.getMovieList();
		
		Assert.assertEquals(actualMovies, expectedMovies);
	}

	@Test
	public void getMovieTest() {
		
		Movie expectedMovie = new Movie("The Shawshank Redemption", Duration.ofMinutes(142), Arrays.asList(Genre.DRAMA), 9.3);
		
		Movie actualMovie = movieRepository.getMovie("The Shawshank Redemption");
		
		Assert.assertEquals(actualMovie, expectedMovie);
	}

	@Test
	public void removeMovieTest() {
		
		movieRepository.removeMovie("The Shawshank Redemption");
		Assert.assertTrue(movieRepository.getMovieList().isEmpty(), "Movie list should be empty");
	}

	@Test
	public void updateMovieTest() {
		
		Movie movie = new Movie("The Shawshank Redemption", Duration.ofMinutes(200), Arrays.asList(Genre.DRAMA, Genre.ACTION, Genre.ROMANCE), 8.1);
		
		movieRepository.updateMovie(movie);
		Movie updatedMovie = movieRepository.getMovie("The Shawshank Redemption");
		
		SoftAssert softAssert = new SoftAssert();
		
		softAssert.assertEquals(updatedMovie.getDuration(), movie.getDuration(), "Duration was not updated");
		softAssert.assertEquals(updatedMovie.getGenres(), movie.getGenres(), "Genres was not updated");
		softAssert.assertEquals(updatedMovie.getRating(), movie.getRating(), "Rating was not updated");
		softAssert.assertAll();
	}
}
