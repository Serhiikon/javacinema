package edu.javasoft.cinema.repositiry;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import edu.javasoft.cinema.model.User;

public class UserRepositoryTest {
	
	private UserRepository userRepository;
	
	@BeforeMethod
	public void beforeMethod() {
		User user = new User("Vasil Palamarchuk", "(066) 122-65-41");
		
		List<User> userList = new ArrayList<>();
		
		userList.add(user);
		
		this.userRepository = new UserRepository();
		this.userRepository.setUserList(userList);
	}

	@Test
	public void addUserTest() {
		
		List<User> expectedUsers = new ArrayList<>();
		
		User user1 = new User("Vasil Palamarchuk", "(066) 122-65-41");
		
		expectedUsers.add(user1);
		
		User user2 = new User("Oleg Andronik", "(066) 966-55-41");
		
		expectedUsers.add(user2);
		
		userRepository.addUser(user2);
		
		List<User> actualUsers = userRepository.getUserList();
		
		Assert.assertEquals(actualUsers, expectedUsers);

	}

	@Test
	public void getUserTest() {
		User expectedUser = new User("Vasil Palamarchuk", "(066) 122-65-41");
		
		User actualUser = userRepository.getUser("(066) 122-65-41");
		
		Assert.assertEquals(actualUser, expectedUser);
	}

	@Test
	public void removeUserTest() {
		userRepository.removeUser("(066) 122-65-41");
		Assert.assertTrue(userRepository.getUserList().isEmpty(), "User list should be empty");
	}

	@Test
	public void updateUserTest() {
		User user = new User("Olexander Yaremchuk", "(066) 122-65-41");
		
		userRepository.updateUser(user);
		User updatedUser = userRepository.getUser("(066) 122-65-41");
		
		Assert.assertEquals(updatedUser.getName(), user.getName(), "Name was not updated");


	}
}
