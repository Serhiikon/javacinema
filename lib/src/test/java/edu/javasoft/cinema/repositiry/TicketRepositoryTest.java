package edu.javasoft.cinema.repositiry;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import edu.javasoft.cinema.model.Genre;
import edu.javasoft.cinema.model.Movie;
import edu.javasoft.cinema.model.Ticket;
import edu.javasoft.cinema.model.User;

public class TicketRepositoryTest {
	
	private TicketRepository ticketRepository;
	
	@BeforeMethod
	public void beforeMethod() {
		
		User user = new User("Vasil Palamarchuk", "(066) 122-65-41");
		
		Movie movie = new Movie("The Shawshank Redemption", Duration.ofMinutes(142), Arrays.asList(Genre.DRAMA), 9.3);
		
		Ticket ticket = new Ticket(UUID.fromString("10b89bfe-ca3c-41c2-93aa-07f26abbd312"), movie, user, LocalDateTime.of(2021, 6, 30, 21, 30), 40.50);
		
		List<Ticket> ticketList = new ArrayList<Ticket>();
		
		ticketList.add(ticket);
		
		this.ticketRepository = new TicketRepository();
		this.ticketRepository.setTicketList(ticketList);
	}

	@Test
	public void addTicketTest() {
		
		List<Ticket> expectedTickets = new ArrayList<>();
		
		User user1 = new User("Vasil Palamarchuk", "(066) 122-65-41");
		
		Movie movie1 = new Movie("The Shawshank Redemption", Duration.ofMinutes(142), Arrays.asList(Genre.DRAMA), 9.3);
		
		Ticket ticket1 = new Ticket(UUID.fromString("10b89bfe-ca3c-41c2-93aa-07f26abbd312"), movie1, user1, LocalDateTime.of(2021, 6, 30, 21, 30), 40.50);
		
		expectedTickets.add(ticket1);
		
		User user2 = new User("Oleg Andronik", "(066) 966-55-41");
		
		Movie movie2 = new Movie("The Shawshank Redemption", Duration.ofMinutes(142), Arrays.asList(Genre.DRAMA), 9.3);
		
		Ticket ticket2 = new Ticket(UUID.randomUUID(), movie2, user2, LocalDateTime.of(2021, 6, 30, 21, 30), 40.50);
		
		expectedTickets.add(ticket2);
		
		ticketRepository.addTicket(ticket2);
		
		List<Ticket> actualTickets = ticketRepository.getTicketList();
		
		Assert.assertEquals(actualTickets, expectedTickets);

	}

	@Test
	public void getTicketTest() {
		
		User user = new User("Vasil Palamarchuk", "(066) 122-65-41");
		
		Movie movie = new Movie("The Shawshank Redemption", Duration.ofMinutes(142), Arrays.asList(Genre.DRAMA), 9.3);
		
		Ticket expectedTicket = new Ticket(UUID.fromString("10b89bfe-ca3c-41c2-93aa-07f26abbd312"), movie, user, LocalDateTime.of(2021, 6, 30, 21, 30), 40.50);
		
		UUID ticketId = UUID.fromString("10b89bfe-ca3c-41c2-93aa-07f26abbd312");
		
		Ticket actualTicket = ticketRepository.getTicket(ticketId);
		
		Assert.assertEquals(actualTicket, expectedTicket);
	}

	@Test
	public void removeTicketTest() {
		
		UUID ticketId = UUID.fromString("10b89bfe-ca3c-41c2-93aa-07f26abbd312");
		ticketRepository.removeTicket(ticketId);
		Assert.assertTrue(ticketRepository.getTicketList().isEmpty(), "Ticket list should be empty");
	}

	@Test
	public void updateTicketTest() {
		
		User user = new User("Oleg Andronik", "(066) 966-55-41");
		
		Movie movie = new Movie("The Dark Knight", Duration.ofMinutes(152), Arrays.asList(Genre.DRAMA, Genre.ACTION), 9.0);
		
		Ticket ticket = new Ticket(UUID.fromString("10b89bfe-ca3c-41c2-93aa-07f26abbd312"), movie, user, LocalDateTime.of(2021, 6, 30, 21, 30), 65.50);
		
		ticketRepository.updateTicket(ticket);
		Ticket updatedTicket = ticketRepository.getTicket(UUID.fromString("10b89bfe-ca3c-41c2-93aa-07f26abbd312"));
		
		SoftAssert softAssert = new SoftAssert();
		
		softAssert.assertEquals(updatedTicket.getMovie(), ticket.getMovie(), "Movie was not updated");
		softAssert.assertEquals(updatedTicket.getUser(), ticket.getUser(), "User was not updated");
		softAssert.assertEquals(updatedTicket.getDate(), ticket.getDate(), "Date was not updated");
		softAssert.assertEquals(updatedTicket.getPrice(), ticket.getPrice(), "Price was not updated");
		softAssert.assertAll();
	}
}
