package edu.javasoft.cinema.service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import edu.javasoft.cinema.model.Genre;
import edu.javasoft.cinema.model.Movie;
import edu.javasoft.cinema.model.Ticket;
import edu.javasoft.cinema.model.User;

public class CinemaServiceTest {
	
	private CinemaService cinemaService;
	
	@BeforeTest
	public void beforeTest() {
		
		User user1 = new User("Vasil Palamarchuk", "(066) 122-65-41");
		
		User user2 = new User("Oleg Andronik", "(066) 966-55-41");
		
		User user3 = new User("Irina Holodniuk", "(066) 155-65-12");
		
		User user4 = new User("Taras Mikitiuk", "(066) 988-65-12");
		
		Movie movie1 = new Movie("The Shawshank Redemption", Duration.ofMinutes(142), Arrays.asList(Genre.DRAMA), 9.3);
		
		Movie movie2 = new Movie("The Godfather", Duration.ofMinutes(175), Arrays.asList(Genre.DRAMA), 9.2);
		
		Movie movie3 = new Movie("The Dark Knight", Duration.ofMinutes(152), Arrays.asList(Genre.DRAMA, Genre.ACTION), 9.0);

		Movie movie4 = new Movie("The Lord of the Rings: The Return of the King", Duration.ofMinutes(201), Arrays.asList(Genre.DRAMA, Genre.ACTION), 8.9);
		
		Ticket ticket1 = new Ticket(UUID.fromString("10b89bfe-ca3c-41c2-93aa-07f26abbd312"), movie1, user1, LocalDateTime.of(2021, 6, 30, 21, 30), 40.50);
		
		Ticket ticket2 = new Ticket(UUID.fromString("5900e62c-d4b3-48a7-9a44-b528616b52c7"), movie1, user2, LocalDateTime.of(2021, 6, 30, 21, 30), 40.50);
		
		Ticket ticket3 = new Ticket(UUID.fromString("43210ff5-49be-4ec7-9199-f4a12be874e0"), movie1, user3, LocalDateTime.of(2021, 6, 30, 21, 30), 40.50);
		
		Ticket ticket4 = new Ticket(UUID.fromString("f38be186-942f-43e4-a13b-5ece223c1bb8"), movie1, user4, LocalDateTime.of(2021, 6, 30, 21, 30), 40.50);

		Ticket ticket5 = new Ticket(UUID.fromString("4ce77e86-3d94-4587-b630-7dcbdc5fcb84"), movie2, user1, LocalDateTime.of(2021, 7, 17, 12, 30), 40.50);

		Ticket ticket6 = new Ticket(UUID.fromString("66b31535-e9ba-42e3-888c-376e14ac78c7"), movie2, user2, LocalDateTime.of(2021, 7, 17, 12, 30), 40.50);
		
		Ticket ticket7 = new Ticket(UUID.fromString("d4f23098-9320-4058-87ad-cc286bc7f042"), movie2, user3, LocalDateTime.of(2021, 7, 17, 12, 30), 40.50);
		
		Ticket ticket8 = new Ticket(UUID.fromString("85daec57-f315-4097-bc40-5f0b02d6d5e7"), movie3, user1, LocalDateTime.now(), 40.50);
		
		Ticket ticket9 = new Ticket(UUID.fromString("c00c4887-dab0-4057-9603-3048a7b72c82"), movie3, user2, LocalDateTime.now(), 40.50);
		
		Ticket ticket10 = new Ticket(UUID.fromString("aaa40018-8b9e-4847-896e-389802995f5d"), movie4, user1, LocalDateTime.of(2021, 9, 8, 12, 30), 40.50);
		
		List<Ticket> ticketList = new ArrayList<Ticket>();
		ticketList.add(ticket1);
		ticketList.add(ticket2);
		ticketList.add(ticket3);
		ticketList.add(ticket4);
		ticketList.add(ticket5);
		ticketList.add(ticket6);
		ticketList.add(ticket7);
		ticketList.add(ticket8);
		ticketList.add(ticket9);
		ticketList.add(ticket10);
		
		this.cinemaService = new CinemaService();
		this.cinemaService.setTicketList(ticketList);
	}
	
	@Test
	public void getMonthIncomeTest() {
		double expectedIncom = 162.0;
		double actualIncome = cinemaService.getMonthIncome(2021, 6);
		Assert.assertEquals(actualIncome, expectedIncom);
	}

	@Test
	public void getMoviesSortedByVisitingCountTest() {
		
		List<Movie> expectedMovies = new ArrayList<Movie>();
		
		Movie movie = new Movie("The Shawshank Redemption", Duration.ofMinutes(142), Arrays.asList(Genre.DRAMA), 9.3);
		
		expectedMovies.add(0, movie);
		
		movie = new Movie("The Godfather", Duration.ofMinutes(175), Arrays.asList(Genre.DRAMA), 9.2);
		
		expectedMovies.add(1, movie);
		
		movie = new Movie("The Dark Knight", Duration.ofMinutes(152), Arrays.asList(Genre.DRAMA, Genre.ACTION), 9.0);
		
		expectedMovies.add(2, movie);
		
		movie = new Movie("The Lord of the Rings: The Return of the King", Duration.ofMinutes(201), Arrays.asList(Genre.DRAMA, Genre.ACTION), 8.9);
		
		expectedMovies.add(3, movie);
		
		List<Movie> actualMovies = cinemaService.getMoviesSortedByVisitingCount();
		
		Assert.assertEquals(actualMovies, expectedMovies);
	}

	@Test
	public void getMoviesThatLessCertainTicketAmountTest() {
		
		List<Movie> expectedMovies = new ArrayList<Movie>();
		
		Movie movie = new Movie("The Dark Knight", Duration.ofMinutes(152), Arrays.asList(Genre.DRAMA, Genre.ACTION), 9.0);
		
		expectedMovies.add(movie);
		
		movie = new Movie("The Lord of the Rings: The Return of the King", Duration.ofMinutes(201), Arrays.asList(Genre.DRAMA, Genre.ACTION), 8.9);
		
		expectedMovies.add(movie);
		
		List<Movie> actualMovies = cinemaService.getMoviesThatLessCertainTicketAmount(3);
		
		Assert.assertEquals(actualMovies, expectedMovies);
	}

	@Test
	public void getTodayMoviesTest() {
		
		List<Movie> expectedMovies = new ArrayList<Movie>();
		
		Movie movie = new Movie("The Dark Knight", Duration.ofMinutes(152), Arrays.asList(Genre.DRAMA, Genre.ACTION), 9.0);
		
		expectedMovies.add(movie);
		
		List<Movie> actualMovies = cinemaService.getTodayMovies();
		
		Assert.assertEquals(actualMovies, expectedMovies);
	}

	@Test
	public void getUsersByDateAndMovieTest() {
		
		List<User> expectedUsers = new ArrayList<User>();
		
		User user = new User("Irina Holodniuk", "(066) 155-65-12");
		
		expectedUsers.add(user);
		
		user = new User("Oleg Andronik", "(066) 966-55-41");
		
		expectedUsers.add(user);
		
		user = new User("Taras Mikitiuk", "(066) 988-65-12");
		
		expectedUsers.add(user);
		
		user = new User("Vasil Palamarchuk", "(066) 122-65-41");
		
		expectedUsers.add(user);
		
		Movie movie = new Movie("The Shawshank Redemption", Duration.ofMinutes(142), Arrays.asList(Genre.DRAMA), 9.3);
		
		List<User> actualUsers = cinemaService.getUsersByDateAndMovie(movie, LocalDate.of(2021, 6, 30));
		
		Assert.assertEquals(actualUsers, expectedUsers);
	}
}
